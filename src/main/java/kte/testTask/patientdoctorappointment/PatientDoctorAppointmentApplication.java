package kte.testTask.patientdoctorappointment;

import kte.testTask.patientdoctorappointment.model.Doctor;
import kte.testTask.patientdoctorappointment.model.Patient;
import kte.testTask.patientdoctorappointment.model.UserAccount;
import kte.testTask.patientdoctorappointment.repository.DoctorRepository;
import kte.testTask.patientdoctorappointment.repository.PatientRepository;
import kte.testTask.patientdoctorappointment.repository.UserAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;


@SpringBootApplication
public class PatientDoctorAppointmentApplication implements CommandLineRunner {
	@Autowired private UserAccountRepository userAccountRepository;
	@Autowired private DoctorRepository doctorRepository;
	@Autowired private PatientRepository patientRepository;
	@Autowired private PasswordEncoder encoder;

	public static void main(String[] args) {
		SpringApplication.run(PatientDoctorAppointmentApplication.class, args);
	}

	@Override
	public void run(String... args){
		UserAccount admin = new UserAccount(
				"admin", encoder.encode("admin"),
				List.of("ROLE_ADMIN"),
				true
		);
		userAccountRepository.save(admin);
		UserAccount user = new UserAccount(
				"user", encoder.encode("user"),
				List.of("ROLE_USER"),
				true
		);
		userAccountRepository.save(user);
		userAccountRepository.findAll().forEach(System.out::println);
		Doctor Therapist = new Doctor("Vasily");
		doctorRepository.save(Therapist);
		LocalDate birthday1 = LocalDate.of(1990, Month.FEBRUARY, 5);
		Patient Vanya = new Patient("Vanya", birthday1);
		patientRepository.save(Vanya);
		System.out.println("Приложение запущено.");
	}
}
