package kte.testTask.patientdoctorappointment.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "tickets")
public class Ticket {
    @Id
    @GeneratedValue
    @Column(name = "Id", nullable = false)
    private Long id;
    @ManyToOne(fetch = FetchType.EAGER)
    private Doctor doctorId;
    @ManyToOne(fetch = FetchType.EAGER)
    private Patient patientId;
    private LocalDateTime visitStart;
    @JsonIgnore
    private LocalDate selectedDay;

    public Ticket() {
    }

    public Long getId() {
        return id;
    }


    public Doctor getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Doctor doctorId) {
        this.doctorId = doctorId;
    }

    public Patient getPatientId() {
        return patientId;
    }

    public void setPatientId(Patient patientId) {
        this.patientId = patientId;
    }

    public LocalDateTime getVisitStart() {
        return visitStart;
    }

    public void setVisitStart(LocalDateTime visitStart) {
        this.visitStart = visitStart;
    }


    public LocalDate getSelectedDay() {
        return selectedDay;
    }

    public void setSelectedDay(LocalDate selectedDay) {
        this.selectedDay = selectedDay;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", doctorId=" + doctorId +
                ", patientId=" + patientId +
                ", visitStart=" + visitStart +
                ", selectedDay=" + selectedDay +
                '}';
    }
}
