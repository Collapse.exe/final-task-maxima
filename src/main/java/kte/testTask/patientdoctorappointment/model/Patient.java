package kte.testTask.patientdoctorappointment.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import org.hibernate.annotations.UuidGenerator;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Entity
@IdClass(CompositeId.class)
@Table(name = "Patients")
public class Patient{
    @Id
    @GeneratedValue
    @Column(name = "Id", nullable = false)
    private Long id;
    @Id
    @GeneratedValue
    @UuidGenerator
    private UUID uuid;
    private String name;
    @OneToMany(mappedBy = "patientId", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Ticket> ticketId;
    private LocalDate birthdayDate;

    public Patient() {
    }

    public Patient(String name, LocalDate birthdayDate) {
        this.name = name;
        this.birthdayDate = birthdayDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonIgnore
    public List<Ticket> getTicketId() {
        return ticketId;
    }

    public void setTicketId(List<Ticket> ticketId) {
        this.ticketId = ticketId;
    }

    public LocalDate getBirthdayDate() {
        return birthdayDate;
    }

    public void setBirthdayDate(LocalDate birthdayDate) {
        this.birthdayDate = birthdayDate;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "id=" + id +
                ", uuid=" + uuid +
                ", name='" + name + '\'' +
             //   ", ticketId=" + ticketId +
                ", birthdayDate=" + birthdayDate +
                '}';
    }
}
