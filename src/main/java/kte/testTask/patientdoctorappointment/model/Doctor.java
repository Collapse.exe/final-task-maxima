package kte.testTask.patientdoctorappointment.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import org.hibernate.annotations.UuidGenerator;

import java.util.List;
import java.util.UUID;

@Entity
@IdClass(CompositeId.class)
@Table(name = "Doctors")
public class Doctor{
@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "Id", nullable = false)
    private Long id;
    @Id
    @GeneratedValue
    @UuidGenerator
    private UUID uuid;
    private String name;
    @OneToMany(mappedBy = "doctorId", cascade = CascadeType.ALL)
    private List<Ticket> ticketId;

    public Doctor() {
    }

    public Doctor(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonIgnore
    public List<Ticket> getTicketId() {
        return ticketId;
    }

    public void setTicketId(List<Ticket> ticketId) {
        this.ticketId = ticketId;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "id=" + id +
                ", uuid=" + uuid +
                ", name='" + name + '\'' +
               // ", ticketId=" + ticketId +
                '}';
    }
}
