package kte.testTask.patientdoctorappointment.exceptions;

public class ModelNotFoundException extends RuntimeException{
    public ModelNotFoundException(String message) {
        super(String.format("Объект с id %s не найден в базе", message));
    }
}
