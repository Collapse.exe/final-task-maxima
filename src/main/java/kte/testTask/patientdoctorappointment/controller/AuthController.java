package kte.testTask.patientdoctorappointment.controller;

import kte.testTask.patientdoctorappointment.model.JwtToken;
import kte.testTask.patientdoctorappointment.service.AuthTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthController {
    @Autowired private AuthTokenService tokenService;

    @PostMapping("/auth")
    public JwtToken hello(Authentication auth){
        return new JwtToken(tokenService.generateToken(auth));
    }
}
