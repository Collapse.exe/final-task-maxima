package kte.testTask.patientdoctorappointment.controller;

import kte.testTask.patientdoctorappointment.model.Ticket;
import kte.testTask.patientdoctorappointment.model.WorkingDayInfo;
import kte.testTask.patientdoctorappointment.service.TimeTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/admin")
public class TimeTableController {
    @Autowired
    TimeTableService ttService;

    @PostMapping("/create")
    public ResponseEntity<Ticket> createTimeTable(@RequestBody WorkingDayInfo info){
        ttService.createTimeTable(info);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
