package kte.testTask.patientdoctorappointment.repository;

import kte.testTask.patientdoctorappointment.model.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAccountRepository extends JpaRepository<UserAccount, String> {
}
