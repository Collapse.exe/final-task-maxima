package kte.testTask.patientdoctorappointment.service;

import kte.testTask.patientdoctorappointment.model.Ticket;
import kte.testTask.patientdoctorappointment.model.WorkingDayInfo;
import kte.testTask.patientdoctorappointment.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.*;

@Service
public class TimeTableService {

    @Autowired
    private TicketRepository ticketRepository;

    public void createTimeTable(WorkingDayInfo info){
        ticketRepository.deleteAll();
        LocalDateTime startDate = info.getStartDate();
        while (startDate.isBefore(info.getEndDate())){
            for (int i = 1; i<=info.getMultiplier(); i++){
                Ticket ticket = new Ticket();
                ticket.setVisitStart(startDate);
                ticket.setSelectedDay(LocalDate.from(startDate));
                ticketRepository.save(ticket);
            }
            startDate = startDate.plusMinutes(info.getDuration().toMinutes());
            if (startDate.getHour() >= info.getEndDate().getHour() && startDate.isBefore(info.getEndDate())){
                startDate = startDate.plusDays(1L);
                startDate = startDate.withHour(info.getStartOfWorkingDay().getHour());
                startDate = startDate.withMinute(info.getStartOfWorkingDay().getMinute());
            }
        }
    }
}
